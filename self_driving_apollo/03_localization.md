* https://github.com/ApolloAuto/apollo/tree/master/modules/localization

# 3. GNSS RTK
* GPS - Global Positioning System
* GNSS - global navigation satellite system

---

* each satellite is equipped with a highly accurate atomic clock

---

* RTK - Real Time kinematic Positioning

![](./fig/03/3.png)

* with RTK -> err less than 10 centimeters

![](./fig/03/3_1.png)

* 10 Hertz update frequency

# 4. inertial navigation
* three axis accelerometer
* 3D gyroscope - Gyro
    * interesting animation of Gyro
* IMU inertial meas unit
    * 1000 Hertz
    * err increases with time

# lidar
* ICP iterative closest point
    * rotate the cloud points -> min. err
* filter algo
    * histogram filter
        * SSD sum of squared diff

![](./fig/03/5.png)

* kalman filter

# Visual localization

![](./fig/03/6.png)
![](./fig/03/6_1.png)

![](./fig/03/6_2.png)
* easy to obtain imgs
* reliance on HP maps, lack of 3d info

# 7 apollo localization
* https://arxiv.org/pdf/1711.05805.pdf

![](./fig/03/7.png)

* you dont lose weight by watching other people exercise