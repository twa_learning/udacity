# 5. how self driving cars work

![](./fig/01/5.png)

![](./fig/01/5_1.png)
* Apollo modules

# 7. reference vehicle and hardware platforms

![](./fig/01/7.png)

* https://github.com/ApolloAuto/apollo/blob/master/docs/quickstart/apollo_2_0_hardware_system_installation_guide_v1.md

* https://github.com/ApolloAuto/apollo

# 8. open software stack
* RTOS
    * ubuntu + Apollo kernel
* runtime framework
    * customized version of ROS
* Modules
    * MAP engine, localization, planning, perception, control, end to end, HMI
        * each module has its own libraries

---

![](./fig/01/8.png)

![](./fig/01/8_1.png)
* many nodes controled by one master node
    * master node fails -> whole sys fail

![](./fig/01/8_2.png)
* decentralized nodes

![](./fig/01/8_3.png)
* protobuf
    * simply add new msg

---

* https://github.com/protocolbuffers/protobuf
    * protobuf instead of ROS msg 
        * ROS msg -> when diff -> two nodes commun. fails

# 9. Cloud service
* http://apollo.auto/platform/security.html
* http://apolloscape.auto/scene.html

---

* HD Map
* Simulation
* Data Platform
* Security
* OTA
* DuerOS
