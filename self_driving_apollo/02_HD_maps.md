* https://github.com/ApolloAuto/apollo/tree/master/modules/map
* the viehcle compare the info collected by its sensor with the HD map to localize itself
* help the perception
    * distant objects (traffic lights)
    * narrow the searching region (region of interests ROI -> traffic signs)
* help path planning

|Main Difference|Standard OpenDRIVE| 	Apollo OpenDRIVE|
|---|---|---|
|Application Scenario|Primarily used in simulation scenarios|Primarily applied to real-world self-driving scenes|
|Elemental Form Expression|Describe lane shape using curve equations and offsets based on reference line|Describe element shapes using absolute coordinate sequences|
|Elemental Richness|Provide common element types such as Road, Junction, Signal, and Object|Refine element expression and enrich element attributes. Such as adding new no-parking areas, crosswalks, deceleration belts, stop lines, parking allowance signs, deceleration allowance signs, etc.|
|Adaptive Driverless Algorithm|N/A| 	Integrate Baidu’s driverless experience to enhance the feasibility and reliability of driverless algorithms.|

![](./fig/02/6.png)

![](./fig/02/6_1.png)

![](./fig/02/6_2.png)

![](./fig/02/6_3.png)

![](./fig/02/6_4.png)